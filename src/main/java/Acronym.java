
public class Acronym {

	private String result = "";

	public Acronym(String phrase) {
		// clean phrase or whitespace, capitalising where needed	
		String cleanPhrase = capitaliseWords(phrase);
		
		String phraseAcronym = "";
		// loop looking for capital letters and add to phraseAcronym
		for (char c: cleanPhrase.toCharArray()) {
			if (String.valueOf(c).equals(String.valueOf(c).toUpperCase())) {
				phraseAcronym += String.valueOf(c);
			}
		}
		result = phraseAcronym;
	}

	public static void main(String[] args) {
		Acronym a = new Acronym("Complementary metal-oxide semiconductor");
		System.out.println("Acronym = " + a.get());
		
	}

	private String capitaliseWords(String phrase) {		
		// trim whitespace and remove non-alpha characters (excluding hyphens)
		String cleanPhrase = phrase.trim().replaceAll("[^a-zA-Z- ]","");
		
		// split phrase on either space or hyphen
		String words[] = cleanPhrase.split(" |-");
		
		StringBuilder sbPhrase = new StringBuilder();
		
		for (String word : words) {
			// check if the word is already all caps. If it is then lower it.
			if (word.equals(word.toUpperCase())) {
				word = word.toLowerCase();
			}
			
			// get the first letter or each word and capitalise
			char firstLetter = Character.toUpperCase(word.charAt(0));
			
			// take the last part of the word for recreating the word with a beginning cap
			String ord = word.substring(1, word.length());
			
			sbPhrase.append(String.valueOf(firstLetter) + ord);
		}
		
		return sbPhrase.toString();
	}

	public String get() {
		return this.result;
	}

}
